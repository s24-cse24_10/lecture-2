#include <iostream>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>

using namespace std;

void drawScene(){
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Objects to be drawn go here

    // Drawing cartesian plane axis
    glLineWidth(2.0f);
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex2f(-1.0f, 0.0f);
        glVertex2f(1.0f, 0.0f);

        glVertex2f(0.0f, 1.0f);
        glVertex2f(0.0f, -1.0f);
    glEnd();

    // Drawing cartesian plane axis ticks
    glLineWidth(1.0f);
    glBegin(GL_LINES);
        for (float x = -0.9f; x < 1.0f; x+= 0.1) {
            glVertex2f(x, 0.025f);
            glVertex2f(x, -0.025f);
        }

        for (float y = -0.9f; y < 1.0f; y+= 0.1) {
            glVertex2f(0.025f, y);
            glVertex2f(-0.025f, y);
        }
    glEnd();

    // Drawing a triangle on top right quadrant
    glColor3f(1.0f, 0.85f, 0.87f);
    glBegin(GL_POLYGON);
        glVertex2f(0.3f, 0.3f);
        glVertex2f(0.7f, 0.3f);
        glVertex2f(0.5f, 0.7f);
    glEnd();

    // Drawing a square on top left quadrant
    glColor3f(0.6f, 0.2f, 0.7f);
    glBegin(GL_POLYGON);
        glVertex2f(-0.3f, 0.3f);
        glVertex2f(-0.7f, 0.3f);
        glVertex2f(-0.7f, 0.7f);
        glVertex2f(-0.3f, 0.7f);
    glEnd();

    // Drawing a circle on bottom left quadrant
    glColor3f(0.2f, 0.5f, 0.9f);
    glBegin(GL_POLYGON);
        float r = 0.2f;
        for (float theta = 0; theta <= 2 * M_PI; theta += 0.01) {
            glVertex2f(r * cos(theta) - 0.5f, r * sin(theta) -0.5f);
        }
    glEnd();

    // Drawing a hexagon on bottom right quadrant
    glColor3f(0.9f, 0.7f, 0.4f);
    glBegin(GL_POLYGON);
        float r2 = 0.2f;
        for (float theta = 0; theta <= 2 * M_PI; theta += (2 * M_PI) / 8) {
            glVertex2f(r2 * cos(theta) + 0.5f, r2 * sin(theta) -0.5f);
        }
    glEnd();

    // drawing 4 points in different quadrants
    glEnable(GL_POINT_SMOOTH);
    glPointSize(20.0f);
    glBegin(GL_POINTS);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(0.5f, 0.5f);

        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(-0.5f, 0.5f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex2f(-0.5f, -0.5f);

        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex2f(0.5f, -0.5f);
    glEnd();

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(400,400);
    glutInitWindowPosition(300, 50);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    // Run the program
    glutMainLoop();

    return 0;
}